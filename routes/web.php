<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::pattern('alias', '[\da-z-]+');

Auth::routes();

Route::post('send-recall', 'RecallController@recall')->name('send.recall');
Route::post('send-subscribe', 'SubscribeController@subscribe')->name('send.subscribe');
Route::post('send-order', 'OrderController@order')->name('send.order');

Route::get('{alias?}', 'PageController@show')->name('page.show');

Route::group(['prefix' => '_root', 'middleware' => 'auth', 'namespace' => 'Admin', 'as' => 'admin.'], static function () {

    Route::get('', 'HomeController@home')->name('home');
    Route::post('search', 'SearchController@search')->name('search');

    Route::post('upload-ckeditor', 'CkeditorController@upload')->name('upload-ckeditor');

    foreach (glob(app_path('Domain/**/routes.php')) as $item) {
        require $item;
    }
});
