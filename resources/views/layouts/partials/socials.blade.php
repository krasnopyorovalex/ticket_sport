<a href="http://vkontakte.ru/ticketgroup" target="_blank" rel="noopener noreferrer">
    <svg>
        <use xlink:href="{{ asset('img/sprites/sprite.svg#vk') }}"></use>
    </svg>
</a>
<a href="http://www.facebook.com/TicketSport" target="_blank" rel="noopener noreferrer">
    <svg>
        <use xlink:href="{{ asset('img/sprites/sprite.svg#fb') }}"></use>
    </svg>
</a>
<a href="https://www.instagram.com/ticketsport" target="_blank" rel="noopener noreferrer">
    <svg>
        <use xlink:href="{{ asset('img/sprites/sprite.svg#insta') }}"></use>
    </svg>
</a>
