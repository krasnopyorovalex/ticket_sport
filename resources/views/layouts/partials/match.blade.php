<div class="match_schedule-row">
    <div class="time">
        <svg>
            <use xlink:href="{{ asset('img/sprites/sprite.svg#timer') }}"></use>
        </svg>
        {{ $match->date }}
        @if($match->time)
        <span>{{ $match->time }}</span>
        @endif
    </div>
    <div class="commands">
        <div>
            <span>{{ $match->teamFirst->name }}</span>
            @if($match->teamFirst->image)
            <img src="{{ $match->teamFirst->image->path }}" alt="{{ $match->teamFirst->image->alt }}" title="{{ $match->teamFirst->image->title }}">
            @endif
        </div>
        -
        <div>
            @if($match->teamSecond && $match->teamSecond->image)
                <img src="{{ $match->teamSecond->image->path }}" alt="{{ $match->teamSecond->image->alt }}" title="{{ $match->teamSecond->image->title }}">
            @endif
            <span>{{ $match->teamSecond ? $match->teamSecond->name : '?' }}</span>
        </div>
    </div>
    <div class="location">
        <svg>
            <use xlink:href="{{ asset('img/sprites/sprite.svg#field') }}"></use>
        </svg>
        <div>
            {{ $match->stadium ? $match->stadium->location : ''}}
            <span>Стадион: {{ $match->stadium ? $match->stadium->name : '' }}</span>
        </div>
    </div>
    <div class="order">
        <div class="btn btn_order" data-match="{{ $match->id }}">
            <svg>
                <use xlink:href="{{ asset('img/sprites/sprite.svg#tickets') }}"></use>
            </svg>
            Билеты
        </div>
    </div>
</div>
