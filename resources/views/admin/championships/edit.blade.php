@extends('layouts.admin')

@section('breadcrumb')
    <li><a href="{{ route('admin.championships.index') }}">Чемпионаты</a></li>
    <li class="active">Форма редактирования чемпионата</li>
@endsection

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Форма редактирования чемпионата</div>

        <div class="panel-body">

            @include('layouts.partials.errors')

            <form action="{{ route('admin.championships.update', ['id' => $championship->id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')

                <div class="row">
                    <div class="col-md-12">
                        @input(['name' => 'name', 'label' => 'Название', 'entity' => $championship])
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        @if ($championship->image)
                            <div class="panel panel-flat border-blue border-xs" id="image__box">
                                <div class="panel-body">
                                    <img src="{{ asset($championship->image->path) }}" alt="" class="upload__image">

                                    <div class="btn-group btn__actions">
                                        <button data-toggle="modal" data-target="#modal_info" type="button" class="btn btn-primary btn-labeled btn-sm"><b><i class="icon-pencil4"></i></b> Атрибуты</button>

                                        <button type="button" data-href="{{ route('admin.images.destroy', ['id' => $championship->image->id]) }}" class="btn delete__img btn-danger btn-labeled btn-labeled-right btn-sm">Удалить <b><i class="icon-trash"></i></b></button>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @imageInput(['name' => 'image', 'type' => 'file', 'entity' => $championship, 'label' => 'Выберите изображение на компьютере'])
                        @submit_btn()
                    </div>
                </div>
            </form>

        </div>
    </div>
    @if ($championship->image)
        @include('layouts.partials._image_attributes_popup', ['image' => $championship->image])
    @endif
@endsection
