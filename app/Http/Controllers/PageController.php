<?php

namespace App\Http\Controllers;

use App\Championship;
use App\Domain\Championship\Queries\GetAllChampionshipsQuery;
use App\Domain\Match\Queries\GetAllPopularMatchesQuery;
use App\Domain\Slider\Queries\GetSliderByIdQuery;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class PageController
 * @package App\Http\Controllers
 */
class PageController extends Controller
{
    /**
     * @return Factory|View
     */
    public function show()
    {
        $popularMatches = $this->dispatch(new GetAllPopularMatchesQuery);
        $championships = $this->dispatch(new GetAllChampionshipsQuery);
        $slider = $this->dispatch(new GetSliderByIdQuery(1));

        $championship = $championships->first();

        return view('page.index', [
            'popularMatches' => $popularMatches,
            'championships' => $championships,
            'slider' => $slider,
            'matches' => $championship->matches()->with(['teamFirst', 'teamSecond', 'stadium'])->active()->paginate(Championship::LIMIT_MATCHES)
        ]);
    }
}
