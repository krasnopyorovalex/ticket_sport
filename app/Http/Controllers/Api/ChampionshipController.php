<?php

namespace App\Http\Controllers\Api;

use App\Championship;
use App\Domain\Championship\Queries\GetChampionshipByIdQuery;
use App\Domain\Championship\Queries\GetChampionshipByIdWithMatchesQuery;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class ChampionshipController
 * @package App\Http\Controllers\Api
 */
class ChampionshipController extends Controller
{
    public function championshipMatches(int $championship)
    {
        $championship = $this->dispatch(new GetChampionshipByIdQuery($championship));

        $matches = $championship->matches()->active()->paginate(Championship::LIMIT_MATCHES);

        return view('layouts.sections.championship_matches', [
            'matches' => $matches
        ]);
    }

    /**
     * @param int $championship
     * @param int $team
     * @return Factory|View|string
     */
    public function championshipTeamMatches(int $championship, int $team)
    {
        $championship = $this->dispatch(new GetChampionshipByIdWithMatchesQuery($championship, $team));

        return view('layouts.sections.matches', ['matches' => $championship->matches]);
    }

}
