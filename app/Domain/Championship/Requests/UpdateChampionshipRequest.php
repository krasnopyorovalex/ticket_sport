<?php

namespace Domain\Championship\Requests;

use App\Http\Requests\Request;

/**
 * Class UpdateChampionshipRequest
 * @package Domain\Championship\Requests
 */
class UpdateChampionshipRequest extends Request
{
    public function rules(): array
    {
        return [
            'name' => 'bail|required|max:255',
            'image' => 'image',
            'imageAlt' => 'string|max:255',
            'imageTitle' => 'string|max:255'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'name.required' => 'Поле «Название» обязательно для заполнения'
        ];
    }
}
