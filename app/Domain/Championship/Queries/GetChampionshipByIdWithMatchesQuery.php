<?php

namespace App\Domain\Championship\Queries;

use App\Championship;

/**
 * Class GetChampionshipByIdWithMatchesQuery
 * @package App\Domain\Championship\Queries
 */
class GetChampionshipByIdWithMatchesQuery
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $team;

    /**
     * GetChampionshipByIdWithMatchesQuery constructor.
     * @param int $id
     * @param int $team
     */
    public function __construct(int $id, int $team)
    {
        $this->id = $id;
        $this->team = $team;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $team = $this->team;

        return Championship::with(['image', 'matches' => static function ($query) use($team) {
            return $query->active()->where(static function ($query) use ($team) {
                return $query->where('team_first_id', $team)
                    ->orWhere('team_second_id', $team);
            });
        }])->findOrFail($this->id);
    }
}
