<?php

namespace App\Domain\Match\Queries;

use App\Championship;
use App\Match;

/**
 * Class GetAllMatchesQuery
 * @package App\Domain\Match\Queries
 */
class GetAllMatchesQuery
{
    private $championship;
    /**
     * @var bool
     */
    private $paginate;

    /**
     * GetAllMatchesQuery constructor.
     * @param Championship $championship
     * @param bool $paginate
     */
    public function __construct(Championship $championship, bool $paginate = false)
    {
        $this->championship = $championship;
        $this->paginate = $paginate;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $query = Match::whereChampionshipId($this->championship->id)->with(['teamFirst', 'teamSecond', 'stadium'])
            ->orderBy('start_datetime', 'desc');
        if ($this->paginate) {
            return $query->paginate(Match::LIMIT_MATCHES);
        }

        return $query->get();
    }
}
