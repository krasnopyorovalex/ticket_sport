<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Championship extends Model
{
    public const LIMIT_MATCHES = 10;

    public $timestamps = false;

    protected $guarded = ['image'];

    private $teams = [];

    /**
     * @return MorphOne
     */
    public function image(): MorphOne
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    /**
     * @return HasMany
     */
    public function matches(): HasMany
    {
        return $this->hasMany(Match::class);
    }

    /**
     * @return array
     */
    public function getTeamsAttribute(): array
    {
        $activeMatches = $this->matches()
            ->with(['teamFirst.image', 'teamSecond.image'])
            ->active()
            ->get();

        if ($activeMatches) {

            foreach ($activeMatches as $match) {

                $this->teams[$match->teamFirst->id] = $match->teamFirst;

                $match->teamSecond ?
                    $this->teams[$match->teamSecond->id] = $match->teamSecond
                    : false;
            }
        }

        return $this->teams;
    }
}
